package com.alquilermaridos

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MaridosDeAlquilerApp: Application()