package com.alquilermaridos.domain.usecases.reviews

import com.alquilermaridos.domain.model.review.HusbandReviewData
import com.alquilermaridos.domain.repository.ReviewRepository
import com.alquilermaridos.util.Resource

class GetHusbandReviewsImpl(
    private val repository: ReviewRepository
) : GetHusbandReviews {
    override suspend fun invoke(husbandId : String): Resource<List<HusbandReviewData>> {
        return repository.getHusbandReviews(husbandId)
    }
}