package com.alquilermaridos.domain.usecases.services

import com.alquilermaridos.domain.model.services.ServiceData
import com.alquilermaridos.domain.model.services.ServiceCreationResponse
import com.alquilermaridos.util.Resource

interface CreateService {
    suspend operator fun invoke(serviceData: ServiceData): Resource<ServiceCreationResponse>
}