package com.alquilermaridos.domain.usecases.reviews

import com.alquilermaridos.domain.model.review.HusbandReviewData
import com.alquilermaridos.util.Resource

interface GetHusbandReviews {
    suspend operator fun invoke(husbandId : String) : Resource<List<HusbandReviewData>>
}