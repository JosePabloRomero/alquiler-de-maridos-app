package com.alquilermaridos.domain.usecases.services

import com.alquilermaridos.domain.model.services.ServiceData
import com.alquilermaridos.domain.model.services.ServiceCreationResponse
import com.alquilermaridos.domain.repository.ServiceRepository
import com.alquilermaridos.util.Resource

class CreateServiceImpl(private val repository: ServiceRepository) : CreateService {
    //TODO("The validations that are at the VM should be in the use cases")
    override suspend fun invoke(serviceData: ServiceData) : Resource<ServiceCreationResponse> {
        return repository.postService(serviceData)
    }
}