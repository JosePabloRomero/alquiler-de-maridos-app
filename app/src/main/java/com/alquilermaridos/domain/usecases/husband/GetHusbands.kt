package com.alquilermaridos.domain.usecases.husband

import com.alquilermaridos.domain.model.husband.HusbandData
import com.alquilermaridos.util.Resource

interface GetHusbands {
    suspend operator fun invoke() : Resource<List<HusbandData>>
}