package com.alquilermaridos.domain.usecases.husband

import com.alquilermaridos.domain.model.husband.HusbandData
import com.alquilermaridos.domain.model.services.OrderData
import com.alquilermaridos.util.Resource

interface GetHusband {
    suspend operator fun invoke(husbandId: String) : Resource<HusbandData>
}