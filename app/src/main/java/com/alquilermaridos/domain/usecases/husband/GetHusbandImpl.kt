package com.alquilermaridos.domain.usecases.husband

import com.alquilermaridos.domain.model.husband.HusbandData
import com.alquilermaridos.domain.repository.HusbandRepository
import com.alquilermaridos.util.Resource

class GetHusbandImpl(private val repository: HusbandRepository) : GetHusband {
    override suspend fun invoke(husbandId: String): Resource<HusbandData> {
        return repository.getSingleHusbands(husbandId)
    }
}