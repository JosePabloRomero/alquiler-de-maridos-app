package com.alquilermaridos.domain.usecases.services

import com.alquilermaridos.domain.model.services.OrderData
import com.alquilermaridos.util.Resource

interface GetUserServices {
    suspend operator fun invoke(userId: String) : Resource<List<OrderData>>
}