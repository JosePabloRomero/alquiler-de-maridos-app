package com.alquilermaridos.domain.usecases.services

import com.alquilermaridos.domain.model.services.OrderData
import com.alquilermaridos.domain.repository.ServiceRepository
import com.alquilermaridos.util.Resource

class GetCustomerServicesImpl (private val repository: ServiceRepository) : GetUserServices {
    override suspend fun invoke(userId: String): Resource<List<OrderData>> {
        return repository.getUserActiveServices(userId)
    }
}