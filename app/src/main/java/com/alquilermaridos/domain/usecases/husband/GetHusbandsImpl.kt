package com.alquilermaridos.domain.usecases.husband

import com.alquilermaridos.domain.model.husband.HusbandData
import com.alquilermaridos.domain.repository.HusbandRepository
import com.alquilermaridos.util.Resource

class GetHusbandsImpl (private val repository: HusbandRepository): GetHusbands {
    override suspend operator fun invoke(): Resource<List<HusbandData>> {
        return repository.getHusbandsData()
    }
}