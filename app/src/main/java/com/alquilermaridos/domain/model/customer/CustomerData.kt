package com.alquilermaridos.domain.model.customer

data class CustomerData(
    val id: String,
    val name: String,
    val phoneNumber: String,
    val email: String,
)
