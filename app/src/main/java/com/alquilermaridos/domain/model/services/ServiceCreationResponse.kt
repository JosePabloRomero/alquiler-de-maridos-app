package com.alquilermaridos.domain.model.services

data class ServiceCreationResponse(
    val id: String? = null,
    val msg: String? = null,
)
