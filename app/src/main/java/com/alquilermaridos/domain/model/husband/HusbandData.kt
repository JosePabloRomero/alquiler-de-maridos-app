package com.alquilermaridos.domain.model.husband

import com.alquilermaridos.domain.model.category.CategoryType

data class HusbandData(
    val id: String,
    val name: String,
    val phoneNumber: String,
    val email: String,
    val description: String,
    val categories: List<CategoryType>
)
