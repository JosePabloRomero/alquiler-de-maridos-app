package com.alquilermaridos.domain.model.services


data class OrderData(
    val date: String,
    val time: String,
    val details: String,
    val address: String,
    val idCategory: String,
    val idCustomer: String,
    val idHusband: String,
    val isActive: Boolean = true,
)