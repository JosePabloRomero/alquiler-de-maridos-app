package com.alquilermaridos.domain.model.category

import androidx.annotation.DrawableRes
import androidx.compose.ui.graphics.Color
import com.alquilermaridos.presentation.ui.theme.*

sealed class CategoryType(
    val name: String,
    val backgroundColor: Color
) {
    object Carpenter : CategoryType(
        name = "Carpintero",
        backgroundColor = Celadon600
    )
    object Electrician : CategoryType(
        name = "Electricista",
        backgroundColor = MaximumBluePurple200
    )
    object Painter : CategoryType(
        name = "Pintor",
        backgroundColor = BananaMania400
    )
    object ConstructionWorker : CategoryType(
        name = "Albañil",
        backgroundColor = Gunmetal400
    )
    object Locksmith : CategoryType(
        name = "Cerrajero",
        backgroundColor = Celadon800
    )
    object Others : CategoryType(
        name = "Otros",
        backgroundColor = MaximumBluePurple600
    )

    companion object {
        fun fromIdCategory(code: String): CategoryType {
            return when(code) {
                "1020" -> Carpenter
                "1030" -> Electrician
                "1040" -> Painter
                "1050" -> ConstructionWorker
                "1060" -> Locksmith
                else -> Others
            }
        }
    }
}