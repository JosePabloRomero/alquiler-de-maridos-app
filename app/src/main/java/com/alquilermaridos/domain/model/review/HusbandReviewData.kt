package com.alquilermaridos.domain.model.review

data class HusbandReviewData(
    val id: String,
    val idHusband: String,
    val idService: String,
    val starRating: Int,
    val comment: String,
)
