package com.alquilermaridos.domain.repository

import com.alquilermaridos.domain.model.services.OrderData
import com.alquilermaridos.domain.model.services.ServiceData
import com.alquilermaridos.domain.model.services.ServiceCreationResponse
import com.alquilermaridos.util.Resource

interface ServiceRepository {
    suspend fun postService(serviceData: ServiceData) : Resource<ServiceCreationResponse>
    suspend fun getUserActiveServices(userId: String) : Resource<List<OrderData>>
}