package com.alquilermaridos.domain.repository

import com.alquilermaridos.domain.model.review.HusbandReviewData
import com.alquilermaridos.util.Resource

interface ReviewRepository {
    suspend fun getHusbandReviews(husbandId : String) : Resource<List<HusbandReviewData>>
}