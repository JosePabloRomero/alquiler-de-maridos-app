package com.alquilermaridos.domain.repository

import com.alquilermaridos.domain.model.husband.HusbandData
import com.alquilermaridos.util.Resource

interface HusbandRepository {
    suspend fun getHusbandsData() : Resource<List<HusbandData>>
    suspend fun getSingleHusbands(husbandId: String) : Resource<HusbandData>
}