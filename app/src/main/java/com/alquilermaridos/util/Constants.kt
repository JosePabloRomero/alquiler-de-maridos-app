package com.alquilermaridos.util

import com.alquilermaridos.domain.model.customer.CustomerData

class Constants {
    companion object {
        const val BASE_URL = "https://us-central1-api-fb-3b0eb.cloudfunctions.net/app/api/"
        const val GET_HUSBANDS_URL = "husbands"
        const val POST_SERVICE_URL = "service"
        const val GET_CUSTOMER_SERVICES_URL = "services/customers/"
        const val GET_CUSTOMER_ACTIVE_SERVICES_URL = "services/customers/active/"
        const val GET_HUSBAND_REVIEWS = "review/husbands/"
        val USER = CustomerData("01","Jose Pablo", "3196747719", "user@gmail.com")
    }
}