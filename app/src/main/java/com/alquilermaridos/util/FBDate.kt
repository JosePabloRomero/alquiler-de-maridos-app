package com.alquilermaridos.util

import java.io.Serializable

data class FBDate(val _seconds: Long = 0L) : Serializable {
    companion object {
        fun from(map: Map<String, Any>) = object {
            val _seconds: Long by map
            val data = FBDate(_seconds)
        }.data
    }
}
