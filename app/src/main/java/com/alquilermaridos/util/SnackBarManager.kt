package com.alquilermaridos.util

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import java.util.*

data class StringMessage(val id: Long, val message: String)

/**
 * Class responsible for managing Snackbar messages to show on the screen
 */
object SnackBarManager {
    private val _stringMessages: MutableStateFlow<List<StringMessage>> = MutableStateFlow(emptyList())
    val stringMessages: StateFlow<List<StringMessage>> get() = _stringMessages.asStateFlow()

    fun showMessage(message: String) {
        _stringMessages.update { currentMessages ->
            currentMessages + StringMessage(
                id = UUID.randomUUID().mostSignificantBits,
                message = message
            )
        }
    }

    fun setMessageStringShown(messageId: Long) {
        _stringMessages.update { currentMessages ->
            currentMessages.filterNot { it.id == messageId }
        }
    }
}