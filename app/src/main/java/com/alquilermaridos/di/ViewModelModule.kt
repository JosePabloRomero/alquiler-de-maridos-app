package com.alquilermaridos.di

import com.alquilermaridos.domain.repository.HusbandRepository
import com.alquilermaridos.domain.repository.ReviewRepository
import com.alquilermaridos.domain.repository.ServiceRepository
import com.alquilermaridos.domain.usecases.husband.GetHusband
import com.alquilermaridos.domain.usecases.husband.GetHusbandImpl
import com.alquilermaridos.domain.usecases.husband.GetHusbands
import com.alquilermaridos.domain.usecases.husband.GetHusbandsImpl
import com.alquilermaridos.domain.usecases.reviews.GetHusbandReviews
import com.alquilermaridos.domain.usecases.reviews.GetHusbandReviewsImpl
import com.alquilermaridos.domain.usecases.services.*
import com.alquilermaridos.util.SnackBarManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ViewModelModule {
    @Provides
    @Singleton
    fun provideGetHusbands(repository: HusbandRepository): GetHusbands = GetHusbandsImpl(repository)

    @Provides
    @Singleton
    fun provideGetSingleHusband(repository: HusbandRepository): GetHusband = GetHusbandImpl(repository)


    @Provides
    @Singleton
    fun provideCreateService(repository: ServiceRepository): CreateService = CreateServiceImpl(repository)

    @Provides
    @Singleton
    fun provideGetUserServices(repository: ServiceRepository): GetUserServices = GetCustomerServicesImpl(repository)

    @Provides
    @Singleton
    fun provideGetHusbandReviews(repository: ReviewRepository): GetHusbandReviews = GetHusbandReviewsImpl(repository)

    @Provides
    @Singleton
    fun provideSnackbarManager(): SnackBarManager = SnackBarManager
}