package com.alquilermaridos.di

import com.alquilermaridos.data.remote.MaridosDeAlquilerApi
import com.alquilermaridos.data.repository.HusbandRepositoryImpl
import com.alquilermaridos.data.repository.ReviewRepositoryImpl
import com.alquilermaridos.data.repository.ServiceRepositoryImpl
import com.alquilermaridos.domain.repository.HusbandRepository
import com.alquilermaridos.domain.repository.ReviewRepository
import com.alquilermaridos.domain.repository.ServiceRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Provides
    @Singleton
    fun provideHusbandRepository(
        api: MaridosDeAlquilerApi
    ): HusbandRepository = HusbandRepositoryImpl(api)

    @Provides
    @Singleton
    fun provideServicesRepository(
        api: MaridosDeAlquilerApi
    ): ServiceRepository = ServiceRepositoryImpl(api)

    @Provides
    @Singleton
    fun provideReviewRepository(
        api: MaridosDeAlquilerApi
    ): ReviewRepository = ReviewRepositoryImpl(api)
}