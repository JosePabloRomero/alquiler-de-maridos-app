package com.alquilermaridos

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.alquilermaridos.presentation.navigation.composables.MaridosDeAlquilerApp
import com.alquilermaridos.presentation.ui.theme.MaridosDeAlquilerTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    @SuppressLint("UnusedMaterialScaffoldPaddingParameter")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MaridosDeAlquilerTheme {
                MaridosDeAlquilerApp()
            }
        }
    }
}