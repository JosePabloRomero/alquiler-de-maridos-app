package com.alquilermaridos.data.remote.model.husband

import com.squareup.moshi.Json

data class HusbandDataDto(
    @field:Json(name = "id")
    val id: String,
    @field:Json(name = "name")
    val name: String,
    @field:Json(name = "phoneNumber")
    val phoneNumber: String,
    @field:Json(name = "email")
    val email: String,
    @field:Json(name = "description")
    val description: String,
    @field:Json(name = "idCategories")
    val idCategories: List<String>,
)