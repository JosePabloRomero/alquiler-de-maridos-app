package com.alquilermaridos.data.remote

import com.alquilermaridos.data.remote.model.husband.HusbandDataDto
import com.alquilermaridos.data.remote.model.review.HusbandReviewDto
import com.alquilermaridos.data.remote.model.services.OrderDto
import com.alquilermaridos.data.remote.model.services.ServiceDto
import com.alquilermaridos.data.remote.model.services.ServiceCreationResponseDto
import com.alquilermaridos.util.Constants.Companion.GET_CUSTOMER_ACTIVE_SERVICES_URL
import com.alquilermaridos.util.Constants.Companion.GET_HUSBANDS_URL
import com.alquilermaridos.util.Constants.Companion.GET_HUSBAND_REVIEWS
import com.alquilermaridos.util.Constants.Companion.POST_SERVICE_URL
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path


interface MaridosDeAlquilerApi {

    @GET(GET_HUSBANDS_URL)
    suspend fun getHusbandsData() : List<HusbandDataDto>

    @GET("$GET_HUSBANDS_URL/{idHusband}")
    suspend fun getSingleHusband(
        @Path(
            value = "idHusband",
            encoded = true
        ) idHusband: String
    ) : HusbandDataDto

    @POST(POST_SERVICE_URL)
    suspend fun createService(@Body serviceDto: ServiceDto) : ServiceCreationResponseDto

    @GET("$GET_CUSTOMER_ACTIVE_SERVICES_URL{idCustomer}")
    suspend fun getUserActiveServices(
        @Path(
            value = "idCustomer",
            encoded = true
        ) userId: String
    ) : List<OrderDto>

    @GET("$GET_HUSBAND_REVIEWS{idHusband}")
    suspend fun getHusbandReviews(
        @Path(
            value = "idHusband",
            encoded = true
        ) idHusband: String
    ) : List<HusbandReviewDto>
}