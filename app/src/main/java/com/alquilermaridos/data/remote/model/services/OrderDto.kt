package com.alquilermaridos.data.remote.model.services

import com.squareup.moshi.Json

data class OrderDto(
    @field:Json(name = "details")
    val details: String,
    @field:Json(name = "address")
    val address: String,
    @field:Json(name = "date")
    val date: String,
    @field:Json(name = "time")
    val time: String,
    @field:Json(name = "idCategory")
    val idCategory: String,
    @field:Json(name = "idCustomer")
    val idCustomer: String,
    @field:Json(name = "idHusband")
    val idHusband: String,
    @field:Json(name = "isActive")
    val isActive: Boolean,
)
