package com.alquilermaridos.data.remote.model.services

import com.squareup.moshi.Json

data class ServiceCreationResponseDto(
    @field:Json(name = "id")
    val id: String? = null,
    @field:Json(name = "msg")
    val message: String? = null
)
