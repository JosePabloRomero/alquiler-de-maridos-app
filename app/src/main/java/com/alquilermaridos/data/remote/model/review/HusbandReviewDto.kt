package com.alquilermaridos.data.remote.model.review

import com.squareup.moshi.Json

data class HusbandReviewDto(
    @field:Json(name = "id")
    val id: String,
    @field:Json(name = "idHusband")
    val idHusband: String,
    @field:Json(name = "idService")
    val idService: String,
    @field:Json(name = "starRating")
    val starRating: Int,
    @field:Json(name = "comment")
    val comment: String,
)
