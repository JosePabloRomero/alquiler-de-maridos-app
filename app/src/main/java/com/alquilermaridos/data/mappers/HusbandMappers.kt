package com.alquilermaridos.data.mappers

import com.alquilermaridos.data.remote.model.husband.HusbandDataDto
import com.alquilermaridos.domain.model.category.CategoryType
import com.alquilermaridos.domain.model.husband.HusbandData

fun HusbandDataDto.toHusbandDataMap() : HusbandData {
    val categories = idCategories.map { category ->
        CategoryType.fromIdCategory(category)
    }
    return HusbandData(
        id, name, phoneNumber, email, description, categories
    )
}

fun List<HusbandDataDto>.toHusbandDataList(): List<HusbandData> {
    return this.map { it.toHusbandDataMap() }
}