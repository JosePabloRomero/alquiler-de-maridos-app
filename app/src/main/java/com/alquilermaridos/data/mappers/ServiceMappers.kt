package com.alquilermaridos.data.mappers

import com.alquilermaridos.data.remote.model.services.ServiceDto
import com.alquilermaridos.data.remote.model.services.ServiceCreationResponseDto
import com.alquilermaridos.domain.model.services.ServiceData
import com.alquilermaridos.domain.model.services.ServiceCreationResponse

fun ServiceDto.toServiceData() : ServiceData {
    return ServiceData(
        details = details,
        address = address,
        date = date,
        time = time,
        idCategory = idCategory,
        idCustomer = idCustomer,
        idHusband = idHusband,
        isActive = isActive
    )
}

fun ServiceData.toServiceDataDto(): ServiceDto {
    return ServiceDto(
        details = details,
        address = address,
        date = date,
        time = time,
        idCategory = idCategory,
        idCustomer = idCustomer,
        idHusband = idHusband,
        isActive = isActive
    )
}

fun ServiceCreationResponseDto.toServiceResponse() : ServiceCreationResponse {
    return ServiceCreationResponse(
        id = id,
        msg = message
    )
}

fun List<ServiceDto>.toServiceDataList() : List<ServiceData> {
    return this.map { it.toServiceData() }
}