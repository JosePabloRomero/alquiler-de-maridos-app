package com.alquilermaridos.data.mappers

import com.alquilermaridos.data.remote.model.services.OrderDto
import com.alquilermaridos.domain.model.services.OrderData

fun OrderDto.toOrderData() : OrderData {
    return OrderData(
        details = details,
        address = address,
        date = date,
        time = time,
        idCategory = idCategory,
        idCustomer = idCustomer,
        idHusband = idHusband,
        isActive = isActive
    )
}

fun List<OrderDto>.toOrderDataList() : List<OrderData> {
    return this.map { it.toOrderData() }
}