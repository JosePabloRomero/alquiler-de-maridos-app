package com.alquilermaridos.data.mappers

import com.alquilermaridos.data.remote.model.review.HusbandReviewDto
import com.alquilermaridos.domain.model.review.HusbandReviewData

fun HusbandReviewDto.toHusbandReviewData() : HusbandReviewData {
    return HusbandReviewData(
        id = id,
        idHusband = idHusband,
        starRating = starRating,
        idService = idService,
        comment = comment
    )
}

fun List<HusbandReviewDto>.toHusbandReviewDataList() : List<HusbandReviewData> {
    return this.map { it.toHusbandReviewData() }
}