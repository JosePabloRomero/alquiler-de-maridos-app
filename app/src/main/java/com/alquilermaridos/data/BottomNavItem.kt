package com.alquilermaridos.data

import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.vector.ImageVector

data class BottomNavItem(
    val name: String,
    val route: String,
    val icon: ImageVector? = null,
    val icon_resource: Painter? = null,
    val badgeCount: Int = 0
)
