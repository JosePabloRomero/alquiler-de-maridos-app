package com.alquilermaridos.data.repository

import com.alquilermaridos.data.mappers.toHusbandDataList
import com.alquilermaridos.data.mappers.toHusbandDataMap
import com.alquilermaridos.data.mappers.toOrderDataList
import com.alquilermaridos.data.remote.MaridosDeAlquilerApi
import com.alquilermaridos.domain.model.husband.HusbandData
import com.alquilermaridos.domain.repository.HusbandRepository
import com.alquilermaridos.util.Resource
import javax.inject.Inject

class HusbandRepositoryImpl @Inject constructor(
    private val api: MaridosDeAlquilerApi
) : HusbandRepository {
    override suspend fun getHusbandsData(): Resource<List<HusbandData>> {
        return try {
            Resource.Success(
                data = api.getHusbandsData().toHusbandDataList()
            )
        } catch (e: Exception) {
            e.printStackTrace()
            Resource.Error(e.message ?: "Ha ocurrido un error inesperado")
        }
    }

    override suspend fun getSingleHusbands(husbandId: String): Resource<HusbandData> {
        return try {
            Resource.Success(
                data = api.getSingleHusband(idHusband = husbandId).toHusbandDataMap()
            )
        } catch (e: Exception) {
            e.printStackTrace()
            Resource.Error(e.message ?: "Ha ocurrido un error inesperado")
        }
    }
}