package com.alquilermaridos.data.repository


import com.alquilermaridos.data.mappers.toOrderDataList
import com.alquilermaridos.data.mappers.toServiceDataDto
import com.alquilermaridos.data.mappers.toServiceResponse
import com.alquilermaridos.data.mappers.toServiceDataList
import com.alquilermaridos.data.remote.MaridosDeAlquilerApi
import com.alquilermaridos.domain.model.services.OrderData
import com.alquilermaridos.domain.model.services.ServiceData
import com.alquilermaridos.domain.model.services.ServiceCreationResponse
import com.alquilermaridos.domain.repository.ServiceRepository
import com.alquilermaridos.util.Resource
import javax.inject.Inject

class ServiceRepositoryImpl @Inject constructor(
    private val api: MaridosDeAlquilerApi
) : ServiceRepository {
    override suspend fun postService(serviceData: ServiceData): Resource<ServiceCreationResponse> {
        return try {
            Resource.Success(
                data = api.createService(serviceData.toServiceDataDto()).toServiceResponse()
            )
        } catch (e: Exception) {
            e.printStackTrace()
            Resource.Error(e.message ?: "Ha ocurrido un error inesperado")
        }
    }

    override suspend fun getUserActiveServices(userId: String): Resource<List<OrderData>> {
        return try {
            Resource.Success(
                data = api.getUserActiveServices(userId = userId).toOrderDataList()
            )
        } catch (e: Exception) {
            e.printStackTrace()
            Resource.Error(e.message ?: "Ha ocurrido un error inesperado")
        }
    }
}