package com.alquilermaridos.data.repository

import com.alquilermaridos.data.mappers.toHusbandReviewDataList
import com.alquilermaridos.data.remote.MaridosDeAlquilerApi
import com.alquilermaridos.domain.model.review.HusbandReviewData
import com.alquilermaridos.domain.repository.ReviewRepository
import com.alquilermaridos.util.Resource
import javax.inject.Inject

class ReviewRepositoryImpl @Inject constructor(
    private val api: MaridosDeAlquilerApi
) : ReviewRepository {
    override suspend fun getHusbandReviews(husbandId: String): Resource<List<HusbandReviewData>> {
        return try {
            Resource.Success(
                data = api.getHusbandReviews(idHusband = husbandId).toHusbandReviewDataList()
            )
        } catch (e: Exception) {
            e.printStackTrace()
            Resource.Error(e.message ?: "Ha ocurrido un error inesperado")
        }
    }
}