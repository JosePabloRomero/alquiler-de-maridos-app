package com.alquilermaridos.presentation.husband

import com.alquilermaridos.domain.model.husband.HusbandData
import com.alquilermaridos.domain.model.review.HusbandReviewData

data class HusbandDetailsState (
    val reviewsData: List<HusbandReviewData>? = null,
    val husbandData: HusbandData? = null,
    val isLoading: Boolean = false,
    val error: String? = null
)