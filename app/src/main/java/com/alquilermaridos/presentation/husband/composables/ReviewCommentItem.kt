package com.alquilermaridos.presentation.husband.composables

import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.alquilermaridos.domain.model.review.HusbandReviewData
import com.alquilermaridos.presentation.ui.theme.BananaMania400
import com.alquilermaridos.presentation.ui.theme.MaridosDeAlquilerTheme

@Composable
fun ReviewCommentItem(
    modifier: Modifier = Modifier,
    review: HusbandReviewData
) {
    Card(
        modifier = modifier
            .fillMaxWidth()
            .padding(8.dp),
        elevation = 10.dp,
        backgroundColor = MaterialTheme.colors.surface,
    ) {
        Column(
            modifier = Modifier.padding(16.dp)
        ) {
            Row(verticalAlignment = Alignment.CenterVertically) {
                Text(
                    text = review.starRating.toString(),
                    style = MaterialTheme.typography.h5
                )
                Icon(imageVector = Icons.Default.Star, contentDescription = "rating", tint = BananaMania400)
            }
            Text(
                text = review.comment,
                style = MaterialTheme.typography.body1
            )
        }
    }
}

@Preview
@Composable
fun ReviewCommentItemPreview() {
    MaridosDeAlquilerTheme {
        val review = HusbandReviewData(
            "01",
            "01",
            "0231041041",
            4,
            comment = "Hola esto es el comentario a una reseña sjaj"
        )
        ReviewCommentItem(review = review)
    }
}