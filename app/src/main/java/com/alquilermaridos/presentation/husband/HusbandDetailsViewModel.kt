package com.alquilermaridos.presentation.husband

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.alquilermaridos.R
import com.alquilermaridos.di.ResourcesProvider
import com.alquilermaridos.domain.usecases.husband.GetHusband
import com.alquilermaridos.domain.usecases.reviews.GetHusbandReviews
import com.alquilermaridos.util.Resource
import com.alquilermaridos.util.SnackBarManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HusbandDetailsViewModel @Inject constructor(
    private var getHusbandReviews: GetHusbandReviews,
    private var getHusband: GetHusband,
    private val snackbarManager: SnackBarManager,
    private val resourcesProvider: ResourcesProvider
): ViewModel() {

    var state by mutableStateOf(HusbandDetailsState())
        private set

    fun fetchHusband(husbandId: String) {
        viewModelScope.launch {
            state = state.copy(
                isLoading = true
            )
            val getHusbandResult = async { getHusband(husbandId) }
            when (val result = getHusbandResult.await()) {
                is Resource.Success -> {
                    state = state.copy(
                        husbandData = result.data,
                        isLoading = false,
                        error = null
                    )
                }
                is Resource.Error -> {
                    state = state.copy(
                        husbandData = null,
                        isLoading = false,
                        error = result.message
                    )
                    if (result.message != null) {
                        snackbarManager.showMessage(result.message)
                    } else {
                        snackbarManager.showMessage(resourcesProvider.getString(R.string.feed_error))
                    }
                }
            }
        }
    }

    fun fetchReviews(husbandId : String) {
        viewModelScope.launch {
            state = state.copy(
                isLoading = true
            )
            val getHusbandReviewsResult = async { getHusbandReviews(husbandId) }
            when (val result = getHusbandReviewsResult.await()) {
                is Resource.Success -> {
                    state = state.copy(
                        reviewsData = result.data,
                        isLoading = false,
                        error = null
                    )
                }
                is Resource.Error -> {
                    state = state.copy(
                        reviewsData = null,
                        isLoading = false,
                        error = result.message
                    )
                    if (result.message != null) {
                        snackbarManager.showMessage(result.message)
                    } else {
                        snackbarManager.showMessage(resourcesProvider.getString(R.string.reviews_error))
                    }
                }
            }
        }
    }
}