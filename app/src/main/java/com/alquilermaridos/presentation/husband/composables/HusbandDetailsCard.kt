package com.alquilermaridos.presentation.husband.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.alquilermaridos.R
import com.alquilermaridos.domain.model.category.CategoryType
import com.alquilermaridos.domain.model.husband.HusbandData
import com.alquilermaridos.presentation.ui.components.RoundedButton
import com.alquilermaridos.presentation.ui.components.TextChip
import com.alquilermaridos.presentation.ui.theme.MaridosDeAlquilerTheme
import com.alquilermaridos.presentation.ui.theme.MaximumBluePurple200
import com.alquilermaridos.presentation.ui.theme.MidnightGreen400

@Composable
fun HusbandDetailsCard(
    modifier: Modifier = Modifier,
    husband: HusbandData,
    onRequestServiceClick: () -> Unit
) {
    Box(
        modifier = modifier
            .fillMaxWidth()
            .background(MaterialTheme.colors.primary)
    ) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Image(
                painter = painterResource(id = R.drawable.ic_hammer_screwdriver),
                contentDescription = "Profile",
                modifier = Modifier
                    .padding(16.dp)
                    .size(60.dp)
            )
            Column(
                modifier = Modifier.padding(vertical = 12.dp),
            ) {
                Row(
                    modifier
                        .fillMaxWidth()
                        .padding(end = 16.dp),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(
                        text = husband.name,
                        style = MaterialTheme.typography.h5,
                        color = MaterialTheme.colors.onBackground
                    )
                    TextChip(husband.phoneNumber, MidnightGreen400, fontWeight = FontWeight.Bold)
                }

                Text(
                    text = husband.email,
                    style = MaterialTheme.typography.body1.copy(
                        color = MaximumBluePurple200,
                        fontWeight = FontWeight.Bold
                    )
                )

                Text(
                    text = husband.description,
                    style = MaterialTheme.typography.body1
                )
                Row(
                    modifier
                        .fillMaxWidth()
                        .padding(vertical = 6.dp)
                        .padding(bottom = 10.dp)
                ) {
                    husband.categories.forEach { category ->
                        TextChip(category.name, category.backgroundColor)
                    }
                }
                ClickableText(
                    text = buildAnnotatedString {
                        withStyle(
                            style = SpanStyle(
                                color = MaterialTheme.colors.onSurface,
                                fontSize = 16.sp
                            )
                        ) {
                            append("¿Te interesa? ")
                        }
                        withStyle(
                            style = SpanStyle(
                                color = MidnightGreen400,
                                fontWeight = FontWeight.Bold,
                                fontSize = 16.sp
                            )
                        ) {
                            append("Agenda un servicio")
                        }

                    }
                ) {
                    onRequestServiceClick()
                }
            }
        }
    }
}

@Preview
@Composable
fun HusbandDetailsCardPreview() {
    MaridosDeAlquilerTheme {
        val categories = listOf("1020", "1030", "1040")
        val categoriesType = categories.map {
            CategoryType.fromIdCategory(it)
        }
        val husband = HusbandData(
            id = "01",
            name = "Marido de Prueba",
            phoneNumber = "3132031313",
            email = "corrreo@gmail.com",
            description = "Hola me gusta mucho trabajar y soy bueno trabajando",
            categories = categoriesType
        )
        HusbandDetailsCard(husband = husband) {

        }
    }
}