package com.alquilermaridos.presentation.husband.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.alquilermaridos.R
import com.alquilermaridos.domain.model.husband.HusbandData
import com.alquilermaridos.presentation.feed.composables.HusbandItem
import com.alquilermaridos.presentation.husband.HusbandDetailsState
import com.alquilermaridos.presentation.husband.HusbandDetailsViewModel

@Composable
fun HusbandDetailsScreen(
    modifier: Modifier = Modifier,
    viewModel: HusbandDetailsViewModel,
    husbandId: String,
    state: HusbandDetailsState,
    upPress: () -> Unit,
    onRequestServiceClick: (String) -> Unit
) {
    LaunchedEffect(Unit) {
        viewModel.fetchHusband(husbandId)
        viewModel.fetchReviews(husbandId)
    }

    Column(modifier = modifier.fillMaxSize().background(MaterialTheme.colors.secondary)) {
        Row(
            modifier = modifier.height(55.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            IconButton(
                onClick = {
                    upPress()
                }
            ) {
                Icon(
                    imageVector = Icons.Default.ArrowBack,
                    contentDescription = "Back Icon",
                    tint = MaterialTheme.colors.onBackground
                )
            }
            Text(
                text = stringResource(id = R.string.husband_details_title),
                style = MaterialTheme.typography.h3
            )
        }
        if (state.error == null) {
            if (state.husbandData != null) {
                HusbandDetailsCard(husband = state.husbandData) {
                    onRequestServiceClick(husbandId)
                }
            }
            LazyColumn(modifier = modifier
                .fillMaxWidth()
                .height(500.dp)) {
                if (state.reviewsData != null) {
                    itemsIndexed(state.reviewsData) { index, review ->
                        ReviewCommentItem(
                            review = review
                        )
                    }
                }
            }
        }
    }
}