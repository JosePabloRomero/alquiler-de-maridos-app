package com.alquilermaridos.presentation.registration.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import com.alquilermaridos.R
import com.alquilermaridos.presentation.registration.RegistrationState
import com.alquilermaridos.presentation.ui.components.EventDialog
import com.alquilermaridos.presentation.ui.components.RoundedButton
import com.alquilermaridos.presentation.ui.components.SocialMediaButton
import com.alquilermaridos.presentation.ui.components.TransparentTextField
import com.alquilermaridos.presentation.ui.theme.FacebookColor
import com.alquilermaridos.presentation.ui.theme.GmailColor
import com.alquilermaridos.presentation.ui.theme.MidnightGreen400
import com.alquilermaridos.presentation.ui.theme.White300

@Composable
fun RegistrationScreen(
    state: RegistrationState,
    onRegister: (String, String, String, String, String) -> Unit,
    onBack: () -> Unit,
    onDismissDialog: () -> Unit
) {
    val nameValue = remember {
        mutableStateOf("")
    }
    val emailValue = remember {
        mutableStateOf("")
    }
    val phoneValue = remember {
        mutableStateOf("")
    }
    val passwordValue = remember {
        mutableStateOf("")
    }
    val confirmPasswordValue = remember {
        mutableStateOf("")
    }
    var passwordVisibility by remember { mutableStateOf(false) }
    var confirmPasswordVisibility by remember { mutableStateOf(false) }
    val focusManager = LocalFocusManager.current

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .background(MaterialTheme.colors.surface)
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp)
                .verticalScroll(rememberScrollState())
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                IconButton(
                    onClick = {
                        onBack()
                    }
                ) {
                    Icon(
                        imageVector = Icons.Default.ArrowBack,
                        contentDescription = "Back Icon",
                        tint = MaterialTheme.colors.primaryVariant
                    )
                }

                Text(
                    text = stringResource(id = R.string.register_create_account_title),
                    style = MaterialTheme.typography.h2.copy(
                        color = MidnightGreen400
                    )
                )
            }
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.spacedBy(16.dp)
            ) {
                TransparentTextField(
                    textFieldValue = nameValue,
                    textLabel = stringResource(id = com.alquilermaridos.R.string.register_name_label),
                    keyboardType = KeyboardType.Text,
                    keyboardActions = KeyboardActions(
                        onNext = {
                            focusManager.moveFocus(FocusDirection.Down)
                        }
                    ),
                    imeAction = ImeAction.Next
                )
                TransparentTextField(
                    textFieldValue = emailValue,
                    textLabel = stringResource(id = com.alquilermaridos.R.string.register_email_label),
                    keyboardType = KeyboardType.Email,
                    keyboardActions = KeyboardActions(
                        onNext = {
                            focusManager.moveFocus(FocusDirection.Down)
                        }
                    ),
                    imeAction = ImeAction.Next
                )
                TransparentTextField(
                    textFieldValue = phoneValue,
                    textLabel = stringResource(id = com.alquilermaridos.R.string.register_phone_label),
                    maxChar = 10,
                    keyboardType = KeyboardType.Phone,
                    keyboardActions = KeyboardActions(
                        onNext = {
                            focusManager.moveFocus(FocusDirection.Down)
                        }
                    ),
                    imeAction = ImeAction.Next
                )
                TransparentTextField(
                    textFieldValue = passwordValue,
                    textLabel = stringResource(id = com.alquilermaridos.R.string.register_password_label),
                    keyboardType = KeyboardType.Password,
                    keyboardActions = KeyboardActions(
                        onNext = {
                            focusManager.moveFocus(FocusDirection.Down)
                        }
                    ),
                    imeAction = ImeAction.Next,
                    trailingIcon = {
                        IconButton(onClick = {
                            passwordVisibility = !passwordVisibility
                        }) {
                            Icon(
                                painter = if (passwordVisibility) {
                                    painterResource(id = R.drawable.ic_visibility_off)
                                } else {
                                    painterResource(id = R.drawable.ic_visibility)
                                },
                                contentDescription = "Toggle Password Icon"
                            )
                        }
                    },
                    visualTransformation = if (passwordVisibility) {
                        VisualTransformation.None
                    } else {
                        PasswordVisualTransformation()
                    }
                )
                TransparentTextField(
                    textFieldValue = confirmPasswordValue,
                    textLabel = stringResource(id = com.alquilermaridos.R.string.register_confirm_password_label),
                    keyboardType = KeyboardType.Password,
                    keyboardActions = KeyboardActions(
                        onDone = {
                            focusManager.clearFocus()
                            onRegister(
                                nameValue.value,
                                emailValue.value,
                                phoneValue.value,
                                passwordValue.value,
                                confirmPasswordValue.value
                            )
                        }
                    ),
                    imeAction = ImeAction.Done,
                    trailingIcon = {
                        IconButton(onClick = {
                            confirmPasswordVisibility = !confirmPasswordVisibility
                        }) {
                            Icon(
                                painter = if (confirmPasswordVisibility) {
                                    painterResource(id = R.drawable.ic_visibility_off)
                                } else {
                                    painterResource(id = R.drawable.ic_visibility)
                                },
                                contentDescription = "Toggle Confirm Password Icon"
                            )
                        }
                    },
                    visualTransformation = if (confirmPasswordVisibility) {
                        VisualTransformation.None
                    } else {
                        PasswordVisualTransformation()
                    }
                )

                Spacer(modifier = Modifier.height(16.dp))

                RoundedButton(
                    text = stringResource(id = R.string.register_signup),
                    displayProgressBar = state.displayProgressBar,
                    onClick = {
                        onRegister(
                            nameValue.value,
                            emailValue.value,
                            phoneValue.value,
                            passwordValue.value,
                            confirmPasswordValue.value
                        )
                    }
                )

                ClickableText(
                    text = buildAnnotatedString {
                        withStyle(
                            style = SpanStyle(
                                color = MaterialTheme.colors.onSurface,
                            )
                        ) {
                            append(stringResource(id = R.string.register_already_have_account) + " ")
                        }
                        withStyle(
                            style = SpanStyle(
                                color = MidnightGreen400,
                                fontWeight = FontWeight.Bold
                            )
                        ) {
                            append(stringResource(id = R.string.register_login))
                        }
                    }
                ) {
                    onBack()
                }

                Spacer(modifier = Modifier.height(16.dp))

                Column(
                    verticalArrangement = Arrangement.spacedBy(2.dp)
                ) {
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.Center
                    ) {
                        Divider(
                            modifier = Modifier.width(24.dp),
                            thickness = 1.dp,
                            color = White300
                        )
                        Text(
                            modifier = Modifier.padding(8.dp),
                            text = stringResource(id = R.string.register_or_title),
                            style = MaterialTheme.typography.h6.copy(
                                fontWeight = FontWeight.Black,
                                color = White300
                            )
                        )
                        Divider(
                            modifier = Modifier.width(24.dp),
                            thickness = 1.dp,
                            color = White300
                        )
                    }

                    Text(
                        modifier = Modifier.fillMaxWidth(),
                        text = stringResource(id = R.string.register_login_with),
                        style = MaterialTheme.typography.body1.copy(
                            color = MidnightGreen400
                        ),
                        textAlign = TextAlign.Center
                    )
                }

                Spacer(modifier = Modifier.height(16.dp))

                Column(
                    modifier = Modifier.fillMaxWidth(),
                    verticalArrangement = Arrangement.spacedBy(8.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    SocialMediaButton(
                        text = stringResource(id = R.string.social_media_facebook),
                        onClick = {
                            //TODO("FACEBOOK LOGIN")
                        },
                        socialMediaColor = FacebookColor
                    )
                    SocialMediaButton(
                        text = stringResource(id = R.string.social_media_gmail),
                        onClick = {
                            //TODO("FACEBOOK LOGIN")
                        },
                        socialMediaColor = GmailColor
                    )
                }
            }
        }

        if (state.errorMessage != null) {
            EventDialog(errorMessage = state.errorMessage, onDismiss = onDismissDialog)
        }
    }
}