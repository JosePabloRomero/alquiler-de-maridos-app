package com.alquilermaridos.presentation.services

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.alquilermaridos.R
import com.alquilermaridos.domain.model.services.ServiceData
import com.alquilermaridos.domain.usecases.services.CreateService
import com.alquilermaridos.util.Constants.Companion.USER
import com.alquilermaridos.util.Resource
import com.alquilermaridos.util.SnackBarManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ServiceViewModel @Inject constructor(
    private var createService: CreateService,
    private val snackbarManager: SnackBarManager
) : ViewModel() {
    var state: MutableState<ServiceState> = mutableStateOf(ServiceState())
        private set

    fun RequestService(
        details: String,
        address: String,
        idCategory: String,
        idHusband: String,
        date: String,
        time: String
    ) {
        //TODO("Validate fields")
        val errorMessage =
            if (details.isBlank() || date.isBlank() || address.isBlank() || time.isBlank()) {
                R.string.error_input_empty
            } else null

        errorMessage?.let {
            state.value = state.value.copy(
                errorMessage = it
            )
            return
        }

        val serviceData = ServiceData(
            details = details,
            address = address,
            date = date,
            time = time,
            idCategory = idCategory,
            idCustomer = USER.id,
            idHusband = idHusband
        )

        viewModelScope.launch {
            state.value = state.value.copy(
                displayProgressBar = true
            )
            val serviceResponse = async { createService(serviceData) }
            when (val result = serviceResponse.await()) {
                is Resource.Success -> {
                    val serviceId = result.data?.id
                    serviceId?.let {
                        result.data.msg?.let { message -> snackbarManager.showMessage(message) }
                        delay(1000)
                        state.value = state.value.copy(
                            successRequest = true,
                            serviceId = serviceId,
                        )
                    }
                }
                is Resource.Error -> {
                    state.value = state.value.copy(
                        successRequest = false,
                        error = result.message,
                    )
                    result.message?.let { snackbarManager.showMessage(it) }
                }
            }
            state.value = state.value.copy(
                displayProgressBar = false
            )
        }
    }

    fun hideErrorDialog() {
        state.value = state.value.copy(
            errorMessage = null
        )
    }
}