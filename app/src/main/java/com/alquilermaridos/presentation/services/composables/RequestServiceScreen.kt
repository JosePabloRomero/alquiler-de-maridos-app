package com.alquilermaridos.presentation.services.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import com.alquilermaridos.R
import com.alquilermaridos.presentation.services.ServiceState
import com.alquilermaridos.presentation.services.ServiceViewModel
import com.alquilermaridos.presentation.ui.components.*
import com.alquilermaridos.util.FBDate

@Composable
fun RequestServiceScreen(
    husbandId: String,
    upPress: () -> Unit,
    modifier: Modifier = Modifier,
    state: ServiceState,
    viewModel: ServiceViewModel,
) {
    val date = remember {
        mutableStateOf("")
    }

    val time = remember {
        mutableStateOf("")
    }

    val details = remember {
        mutableStateOf("")
    }

    val address = remember {
        mutableStateOf("")
    }
    val focusManager = LocalFocusManager.current
    Box(
        modifier = modifier
            .fillMaxSize()
            .background(MaterialTheme.colors.background),
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(8.dp)
                .verticalScroll(rememberScrollState()),
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                IconButton(
                    onClick = {
                        upPress()
                    }
                ) {
                    Icon(
                        imageVector = Icons.Default.ArrowBack,
                        contentDescription = "Back Icon",
                        tint = MaterialTheme.colors.onBackground
                    )
                }
                Text(
                    text = stringResource(id = R.string.service_request_service_title),
                    style = MaterialTheme.typography.h3
                )
            }
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.spacedBy(16.dp),
            ) {
                TransparentTextField(
                    textFieldValue = details,
                    textLabel = stringResource(id = R.string.services_details_label),
                    keyboardType = KeyboardType.Text,
                    keyboardActions = KeyboardActions(
                        onNext = {
                            focusManager.moveFocus(FocusDirection.Down)
                        }
                    ),
                    imeAction = ImeAction.Next,
                    maxChar = 300
                )
                TransparentTextField(
                    textFieldValue = address,
                    textLabel = stringResource(id = R.string.services_address_label),
                    keyboardType = KeyboardType.Text,
                    keyboardActions = KeyboardActions(
                        onNext = {
                            focusManager.moveFocus(FocusDirection.Down)
                        }
                    ),
                    imeAction = ImeAction.Next,
                    maxChar = 100
                )
                DatePicker(
                    date = date,
                    textLabel = stringResource(id = R.string.services_pick_date_label)
                )
                TimePicker(
                    time = time,
                    textLabel = stringResource(id = R.string.services_pick_time_label)
                )
                Spacer(modifier = Modifier.height(16.dp))
                RoundedButton(
                    text = stringResource(id = R.string.services_request_service),
                    displayProgressBar = state.displayProgressBar,
                    onClick = {
                        viewModel.RequestService(
                            details = details.value,
                            address = address.value,
                            idHusband = husbandId,
                            date = date.value,
                            time = time.value,
                            idCategory = "1020"
                        )
                    }
                )
            }
        }
        if (state.errorMessage != null) {
            EventDialog(
                errorMessage = state.errorMessage,
                onDismiss = { viewModel.hideErrorDialog() }
            )
        }
        if (state.successRequest) {
            upPress()
        }
    }
}