package com.alquilermaridos.presentation.services

import androidx.annotation.StringRes

data class ServiceState(
    val isLoading: Boolean = false,
    val error: String? = null,
    val successRequest: Boolean = false,
    val displayProgressBar: Boolean = false,
    val serviceId: String? = null,
    @StringRes val errorMessage: Int? = null
)