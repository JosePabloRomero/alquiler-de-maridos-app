package com.alquilermaridos.presentation.ui.components

import android.app.DatePickerDialog
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.DateRange
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.alquilermaridos.presentation.ui.theme.White300
import java.util.*

@Composable
fun DatePicker(
    date: MutableState<String>,
    textLabel: String
) {
    val mCalendar = Calendar.getInstance()

    val mDatePickerDialog = DatePickerDialog(
        LocalContext.current,
        { datePickerDialog, mYear: Int, mMonth: Int, mDay: Int ->
            mCalendar[Calendar.YEAR] = mYear
            mCalendar[Calendar.MONTH] = mMonth
            mCalendar[Calendar.DAY_OF_MONTH] = mDay
            date.value = "$mDay/${mMonth + 1}/$mYear"
        }, mCalendar[Calendar.YEAR], mCalendar[Calendar.MONTH], mCalendar[Calendar.DAY_OF_MONTH]
    )
    mCalendar.add(Calendar.DATE, 0)
    mDatePickerDialog.datePicker.minDate = mCalendar.timeInMillis

    mCalendar.add(Calendar.DATE, 30)
    mDatePickerDialog.datePicker.maxDate = mCalendar.timeInMillis

    Box(modifier = Modifier.fillMaxWidth()) {
        Row(modifier = Modifier.align(Alignment.Center)) {
            OutlinedTextField(
                value = date.value,
                onValueChange = {
                    date.value = it
                },
                readOnly = true,
                label = {
                    Text(
                        text = textLabel
                    )
                },
                colors = TextFieldDefaults.textFieldColors(
                    textColor = Color.White,
                    focusedLabelColor = MaterialTheme.colors.onSurface,
                    unfocusedLabelColor = White300,
                    focusedIndicatorColor = MaterialTheme.colors.primaryVariant
                )
            )
            Icon(
                imageVector = Icons.Default.DateRange,
                contentDescription = "date",
                modifier = Modifier
                    .size(68.dp)
                    .padding(4.dp)
                    .clickable {
                        mDatePickerDialog.show()
                    }
            )
        }
    }
}
