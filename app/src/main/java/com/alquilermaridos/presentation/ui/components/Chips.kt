package com.alquilermaridos.presentation.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.material.Text
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion.DarkGray
import androidx.compose.ui.graphics.Color.Companion.LightGray
import androidx.compose.ui.graphics.Color.Companion.Transparent
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.alquilermaridos.presentation.ui.theme.Shapes

@Composable
fun TextChip(
    text: String,
    backgroundColor: Color,
    fontSize: TextUnit? = null,
    fontWeight: FontWeight? = null
) {
    Row(
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .padding(
                vertical = 2.dp,
                horizontal = 4.dp
            )
            .background(
                color = backgroundColor,
                shape = Shapes.medium
            )
            .clip(shape = Shapes.medium)
            .padding(4.dp)
    ) {

        Text(
            text = text,
            style = MaterialTheme.typography.caption.copy(
                fontSize = fontSize?: 12.sp,
                fontWeight = fontWeight?: FontWeight.Normal
            )
        )
    }
}