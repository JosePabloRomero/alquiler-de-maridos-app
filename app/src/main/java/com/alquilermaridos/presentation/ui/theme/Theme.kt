package com.alquilermaridos.presentation.ui.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.runtime.Composable

private val ColorPalette = darkColors(
    primary = MidnightGreen900,
    primaryVariant = MidnightGreen600,
    onPrimary = White50,
    secondary = Gunmetal900,
    secondaryVariant = Gunmetal600,
    onSecondary = White50,
    background = MidnightGreen900,
    onBackground = White50,
    surface = MidnightGreen900,
    onSurface = White50
)

@Composable
fun MaridosDeAlquilerTheme(
    content: @Composable () -> Unit
) {
    val colors = ColorPalette

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}