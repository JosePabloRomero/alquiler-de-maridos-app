package com.alquilermaridos.presentation.ui.theme

import androidx.compose.ui.graphics.Color

val MidnightGreen900 = Color(0xFF004550) //Original
val MidnightGreen800 = Color(0xFF07687c)
val MidnightGreen700 = Color(0xFF0c7b94)
val MidnightGreen600 = Color(0xFF148ead)
val MidnightGreen500 = Color(0xFF189cbf)
val MidnightGreen400 = Color(0xFF28aacb)
val MidnightGreen300 = Color(0xFF49b7d5)
val MidnightGreen200 = Color(0xFF7bcce3)

val Gunmetal900 = Color(0xFF102830) //Original
val Gunmetal800 = Color(0xFF203c46)
val Gunmetal700 = Color(0xFF2d4f5b)
val Gunmetal600 = Color(0xFF3a6270)
val Gunmetal500 = Color(0xFF457180)
val Gunmetal400 = Color(0xFF608593)

val CharlestonGreen900 = Color(0xFF0F272F)
val CharlestonGreen800 = Color(0xFF1e353e)
val CharlestonGreen700 = Color(0xFF2a4752)
val CharlestonGreen600 = Color(0xFF385a66)
val CharlestonGreen500 = Color(0xFF426876)

val Celadon900 = Color(0xFF00712e)
val Celadon800 = Color(0xFF009142)
val Celadon700 = Color(0xFF10a24d)
val Celadon600 = Color(0xFF20b559)
val Celadon500 = Color(0xFF2ac564)
val Celadon400 = Color(0xFF51cf7b)
val Celadon300 = Color(0xFF72d991)
val Celadon200 = Color(0xFF9CE4B0) //Original

val BananaMania400 = Color(0xFFfbc31e)
val BananaMania300 = Color(0xFFfccf49)
val BananaMania200 = Color(0xFFfddb7d)
val BananaMania100 = Color(0xFFfee9b0) //Original

val MaximumBluePurple700 = Color(0xFF5232c9)
val MaximumBluePurple600 = Color(0xFF613ad2)
val MaximumBluePurple500 = Color(0xFF6c3fd8)
val MaximumBluePurple400 = Color(0xFF845be2)
val MaximumBluePurple300 = Color(0xFF9c79ec)
val MaximumBluePurple200 = Color(0xFFb9a0f4) //original
val MaximumBluePurple100 = Color(0xFFd5c6f8)

val White700 = Color(0xFF818181)
val White600 = Color(0xFF979797)
val White500 = Color(0xFFc2c2c2)
val White300 = Color(0xFFf0f0f0)
val White200 = Color(0xFFf5f5f5)
val White100 = Color(0xFFfafafa)
val White50 = Color(0xFFFFFFFF) //original

val FacebookColor = Color(0xFF0c7b94)
val GmailColor = Color(0xFFDB4437)