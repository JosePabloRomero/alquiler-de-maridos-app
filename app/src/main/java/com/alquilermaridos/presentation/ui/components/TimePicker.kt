package com.alquilermaridos.presentation.ui.components

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.DateRange
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.alquilermaridos.R
import com.alquilermaridos.presentation.ui.theme.MaridosDeAlquilerTheme
import com.alquilermaridos.presentation.ui.theme.White300
import java.time.LocalTime
import java.util.*

@Composable
fun TimePicker(
    time: MutableState<String>,
    textLabel : String
) {
    //TODO("Delete this variable if its the same type as the passed through the params")
    var timeDisplayed = remember {
        mutableStateOf("")
    }

    val mCalendar = Calendar.getInstance()

    val mTimePickerDialog = TimePickerDialog(
        LocalContext.current,
        { _, mHour: Int, mMinute: Int ->
            timeDisplayed.value = LocalTime.of(mHour, mMinute).toString()
            mCalendar[Calendar.HOUR] = mHour
            mCalendar[Calendar.MINUTE] = mMinute
            time.value = timeDisplayed.value
        }, mCalendar[Calendar.HOUR], mCalendar[Calendar.MINUTE], true
    )

    Box(modifier = Modifier.fillMaxWidth()) {
        Row(modifier = Modifier.align(Alignment.Center)) {
            OutlinedTextField(
                value = timeDisplayed.value,
                onValueChange = {
                    timeDisplayed.value = it
                },
                readOnly = true,
                label = {
                    Text(
                        text = textLabel
                    )
                },
                colors = TextFieldDefaults.textFieldColors(
                    textColor = Color.White,
                    focusedLabelColor = MaterialTheme.colors.onSurface,
                    unfocusedLabelColor = White300,
                    focusedIndicatorColor = MaterialTheme.colors.primaryVariant
                )
            )
            Icon(
                painter = painterResource(id = R.drawable.ic_clock_outline),
                contentDescription = "time",
                modifier = Modifier
                    .size(68.dp)
                    .padding(4.dp)
                    .clickable {
                        mTimePickerDialog.show()
                    }
            )
        }
    }
}
