package com.alquilermaridos.presentation.navigation.composables

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.material.Scaffold
import androidx.compose.material.SnackbarHost
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.*
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.alquilermaridos.presentation.husband.HusbandDetailsViewModel
import com.alquilermaridos.presentation.husband.composables.HusbandDetailsScreen
import com.alquilermaridos.presentation.login.LoginViewModel
import com.alquilermaridos.presentation.login.composables.LoginScreen
import com.alquilermaridos.presentation.registration.RegistrationViewModel
import com.alquilermaridos.presentation.registration.composables.RegistrationScreen
import com.alquilermaridos.presentation.services.ServiceState
import com.alquilermaridos.presentation.services.ServiceViewModel
import com.alquilermaridos.presentation.services.composables.RequestServiceScreen
import com.alquilermaridos.presentation.ui.components.MaridosDeAlquilerSnackbar
import com.alquilermaridos.presentation.ui.theme.MaridosDeAlquilerTheme

@Composable
fun MaridosDeAlquilerApp() {
    MaridosDeAlquilerTheme {
        val appState = RememberMaridosDeAlquilerAppState()
        Scaffold(
            bottomBar = {
                if (appState.shouldShowBottomBar) {
                    BottomNavigationBar(
                        tabs = appState.bottomBarTabs,
                        currentRoute = appState.currentRoute!!,
                        navigateToRoute = appState::navigateToBottomBarRoute
                    )
                }
            },
            snackbarHost = {
                SnackbarHost(
                    hostState = it,
                    modifier = Modifier.systemBarsPadding(),
                    snackbar = { snackbarData -> MaridosDeAlquilerSnackbar(snackbarData) }
                )
            },
            scaffoldState = appState.scaffoldState
        ) { innerPaddingModifier ->
            NavHost(
                navController = appState.navController,
                startDestination = MainDestinations.LOGIN_ROUTE,
                modifier = Modifier.padding(innerPaddingModifier)
            ) {
                addLogin(navController = appState.navController)
                addRegister(navController = appState.navController)
                navGraph(
                    onHusbandSelected = appState::navigateToHusbandDetail,
                    upPress = appState::upPress,
                    navController = appState.navController
                )
                addServiceRequest(
                    onHusbandSelected = appState::navigateToHusbandDetail,
                    upPress = appState::upPress
                )
            }
        }
    }
}

private fun NavGraphBuilder.navGraph(
    onHusbandSelected: (String, NavBackStackEntry) -> Unit,
    upPress: () -> Unit,
    navController: NavHostController
) {
    navigation(
        route = MainDestinations.FEED_ROUTE,
        startDestination = BottomNavigationSections.FEED.route
    ) {
        addHomeGraph(onHusbandSelected)
    }
    composable(
        "${MainDestinations.HUSBAND_DETAIL_ROUTE}/{${MainDestinations.HUSBAND_ID_KEY}}",
        arguments = listOf(navArgument(MainDestinations.HUSBAND_ID_KEY) {
            type = NavType.StringType
        })
    ) { backStackEntry ->
        val arguments = requireNotNull(backStackEntry.arguments)
        val husbandId = arguments.getString(MainDestinations.HUSBAND_ID_KEY)
        husbandId?.let {
            val husbandDetailsViewModel: HusbandDetailsViewModel = hiltViewModel()
            HusbandDetailsScreen(
                husbandId = it,
                upPress = upPress,
                viewModel = husbandDetailsViewModel,
                state = husbandDetailsViewModel.state,
            ) {
                navController.navigate("${MainDestinations.CREATE_SERVICE_ROUTE}/{${MainDestinations.HUSBAND_ID_KEY}}")
            }
        }
    }
}

private fun NavGraphBuilder.addServiceRequest(
    onHusbandSelected: (String, NavBackStackEntry) -> Unit,
    upPress: () -> Unit
) {
    navigation(
        route = MainDestinations.FEED_ROUTE,
        startDestination = BottomNavigationSections.FEED.route
    ) {
        addHomeGraph(onHusbandSelected)
    }
    composable(
        "${MainDestinations.CREATE_SERVICE_ROUTE}/{${MainDestinations.HUSBAND_ID_KEY}}",
        arguments = listOf(navArgument(MainDestinations.HUSBAND_ID_KEY) {
            type = NavType.StringType
        })
    ) { backStackEntry ->
        val arguments = requireNotNull(backStackEntry.arguments)
        val husbandId = arguments.getString(MainDestinations.HUSBAND_ID_KEY)
        husbandId?.let {
            val serviceViewModel: ServiceViewModel = hiltViewModel()
            RequestServiceScreen(
                husbandId = it,
                upPress = upPress,
                viewModel = serviceViewModel,
                state = serviceViewModel.state.value
            )
        }
    }
}

private fun NavGraphBuilder.addLogin(
    navController: NavHostController
) {
    composable(
        route = MainDestinations.LOGIN_ROUTE
    ) {
        val viewModel: LoginViewModel = hiltViewModel()
        if (viewModel.state.value.successLogin) {
            LaunchedEffect(key1 = Unit) {
                navController.navigate(MainDestinations.FEED_ROUTE) {
                    popUpTo(MainDestinations.LOGIN_ROUTE) {
                        inclusive = true
                    }
                }
            }
        } else {
            LoginScreen(
                state = viewModel.state.value,
                onLogin = viewModel::login,
                onNavigateToRegister = {
                    navController.navigate(MainDestinations.REGISTER_ROUTE)
                },
                onDismissDialog = viewModel::hideErrorDialog
            )
        }
    }
}

private fun NavGraphBuilder.addRegister(
    navController: NavHostController
) {
    composable(
        route = MainDestinations.REGISTER_ROUTE
    ) {
        val viewModel: RegistrationViewModel = hiltViewModel()
        RegistrationScreen(
            state = viewModel.state.value,
            onRegister = viewModel::register,
            onBack = {
                navController.popBackStack()
            },
            onDismissDialog = viewModel::hideErrorDialog
        )
    }
}