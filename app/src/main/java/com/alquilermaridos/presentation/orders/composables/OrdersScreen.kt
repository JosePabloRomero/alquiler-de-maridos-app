package com.alquilermaridos.presentation.orders.composables

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.alquilermaridos.presentation.orders.OrdersViewModel

@Composable
fun OrdersScreen(
    modifier: Modifier = Modifier,
    viewModel: OrdersViewModel
) {
    val state = viewModel.state
    Column(modifier = modifier.fillMaxSize()) {
        if (state.error == null) {
            Surface(
                modifier = modifier.fillMaxSize()
            ) {
                LazyColumn(modifier = modifier.fillMaxSize()) {
                    if (state.ordersData != null) {
                        itemsIndexed(state.ordersData) { index, order ->
                            //TODO("Add onClick listener for future implementations")
                            OrdersItem(
                                order
                            )
                        }
                    }
                }
            }
        }
    }
}