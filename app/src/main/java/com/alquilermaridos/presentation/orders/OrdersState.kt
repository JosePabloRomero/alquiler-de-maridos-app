package com.alquilermaridos.presentation.orders

import com.alquilermaridos.domain.model.services.OrderData

data class OrdersState(
    val isLoading: Boolean = false,
    val ordersData: List<OrderData>? = null,
    val error: String? = null
)
