package com.alquilermaridos.presentation.orders.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.alquilermaridos.R
import com.alquilermaridos.domain.model.category.CategoryType
import com.alquilermaridos.domain.model.services.OrderData
import com.alquilermaridos.domain.model.services.ServiceData
import com.alquilermaridos.presentation.ui.components.TextChip
import com.alquilermaridos.presentation.ui.theme.MaridosDeAlquilerTheme
import com.alquilermaridos.presentation.ui.theme.MaximumBluePurple200
import com.alquilermaridos.presentation.ui.theme.MidnightGreen400
import com.alquilermaridos.presentation.ui.theme.MidnightGreen800

@Composable
fun OrdersItem(order: OrderData, modifier: Modifier = Modifier) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .clickable(onClick = {
                //TODO("Open Service Details")
            }),
        elevation = 10.dp,
        backgroundColor = MaterialTheme.colors.surface,
    ) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Image(
                painter = painterResource(id = R.drawable.ic_hammer_screwdriver),
                contentDescription = "Profile",
                modifier = Modifier
                    .padding(16.dp)
                    .size(60.dp)
            )
            Column(
                modifier = Modifier.padding(vertical = 16.dp)
            ) {
                Row(
                    modifier
                        .fillMaxWidth()
                        .padding(end = 16.dp),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(
                        text = "Fecha: " + order.date,
                        style = MaterialTheme.typography.h5
                    )
                    TextChip(order.time, MidnightGreen400, fontWeight = FontWeight.Bold)
                }

                /*Text(
                    text = order.husbandName,
                    style = MaterialTheme.typography.body1.copy(
                        color = MaximumBluePurple200,
                        fontWeight = FontWeight.Bold
                    )
                )*/

                Text(
                    text = order.details,
                    style = MaterialTheme.typography.body1
                )
                Row(
                    modifier
                        .fillMaxWidth()
                        .padding(vertical = 6.dp)
                        .padding(end = 16.dp),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    val category = CategoryType.fromIdCategory(order.idCategory)
                    TextChip(order.address, category.backgroundColor)
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun OrdersItemPreview() {
    MaridosDeAlquilerTheme {
        val order = OrderData(
            date = "09/09/2022",
            time = "2:00",
            details = "Arreglar el arreglo que esta sin arreglar",
            address = "Calle 25 #49 86",
            idCategory = "1020",
            idCustomer = "01",
            idHusband = "01",
            isActive = true
        )
        OrdersItem(order = order)
    }
}
