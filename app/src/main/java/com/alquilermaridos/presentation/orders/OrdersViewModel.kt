package com.alquilermaridos.presentation.orders

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.alquilermaridos.R
import com.alquilermaridos.di.ResourcesProvider
import com.alquilermaridos.domain.model.services.OrderData
import com.alquilermaridos.domain.model.services.ServiceData
import com.alquilermaridos.domain.usecases.services.GetUserServices
import com.alquilermaridos.util.Constants
import com.alquilermaridos.util.Resource
import com.alquilermaridos.util.SnackBarManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class OrdersViewModel @Inject constructor(
    private var getUserServices: GetUserServices,
    private val snackbarManager: SnackBarManager,
    private val resourcesProvider: ResourcesProvider
): ViewModel() {
    var state by mutableStateOf(OrdersState())
        private set

    //TODO("This variable won't be necessary if it is not actually used")
    private var services: List<OrderData>? = null

    init {
        fetchServices()
    }

    fun fetchServices() {
        viewModelScope.launch {
            state = state.copy(
                isLoading = true
            )
            val userId = Constants.USER.id
            val getUserServicesResult = async { getUserServices(userId) }

            when (val result = getUserServicesResult.await()) {
                is Resource.Success -> {
                    services = result.data
                    state = state.copy(
                        isLoading = false,
                        ordersData = services,
                        error = null
                    )
                }
                is Resource.Error -> {
                    state = state.copy(
                        isLoading = false,
                        ordersData = null,
                        error = result.message
                    )
                    if (result.message != null) {
                        snackbarManager.showMessage(result.message)
                    } else {
                        snackbarManager.showMessage(resourcesProvider.getString(R.string.orders_error))
                    }
                }
            }
        }
    }
}