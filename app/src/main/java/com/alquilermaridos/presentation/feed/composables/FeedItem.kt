package com.alquilermaridos.presentation.feed.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.alquilermaridos.domain.model.husband.HusbandData
import com.alquilermaridos.presentation.ui.components.TextChip

@Composable
fun HusbandItem(husband: HusbandData, modifier: Modifier = Modifier, onHusbandClick: (String) -> Unit) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .clickable(onClick = { onHusbandClick(husband.id) }),
        elevation = 10.dp,
        backgroundColor = MaterialTheme.colors.surface,
    ) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Image(
                painter = painterResource(id = com.alquilermaridos.R.drawable.ic_account),
                contentDescription = "Profile",
                modifier = Modifier
                    .padding(16.dp)
                    .size(60.dp)
            )
            Column(
                modifier = Modifier.padding(vertical = 16.dp)
            ) {
                Text(
                    text = husband.name,
                    style = MaterialTheme.typography.h5
                )
                Text(
                    text = husband.description,
                    style = MaterialTheme.typography.body1
                )
                Row(
                    modifier
                        .fillMaxWidth()
                        .padding(vertical = 6.dp)) {
                    husband.categories.forEach{ category ->
                        TextChip(category.name, category.backgroundColor)
                    }
                }
            }
        }

    }
}
