package com.alquilermaridos.presentation.feed.composables

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.alquilermaridos.presentation.feed.FeedViewModel
import com.alquilermaridos.presentation.ui.components.MaridosDeAlquilerSnackbar

@Composable
fun FeedScreen(
    modifier: Modifier = Modifier,
    onHusbandClick: (String) -> Unit,
    viewModel: FeedViewModel,
) {
    val state = viewModel.state
    Column(modifier = modifier.fillMaxSize()) {
        FeedSearchBar(onSearch = {
            viewModel.filterHusbandsByName(it)
        }, onBackPressed = {
            viewModel.fetchHusbands()
        })
        if (state.error == null) {
            Surface(
                modifier = modifier
                    .fillMaxSize()
            ) {
                LazyColumn(modifier = modifier.fillMaxSize()) {
                    if (state.husbandsData != null) {
                        itemsIndexed(state.husbandsData) { index, husband ->
                            HusbandItem(
                                husband,
                                onHusbandClick = onHusbandClick
                            )
                        }
                    }
                }
            }
        } else {
            Box(modifier = modifier.fillMaxSize().padding(16.dp), contentAlignment = Alignment.Center) {
                Text(text = state.error, style = MaterialTheme.typography.body1)
            }
        }
    }

}