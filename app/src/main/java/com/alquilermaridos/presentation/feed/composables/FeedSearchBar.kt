package com.alquilermaridos.presentation.feed.composables

import androidx.compose.runtime.*
import androidx.compose.ui.text.input.TextFieldValue
import com.alquilermaridos.presentation.ui.components.SearchAppBar

@Composable
fun FeedSearchBar(onSearch: (String) -> Unit, onBackPressed: () -> Unit) {
    var textState by remember { mutableStateOf(TextFieldValue()) }
    SearchAppBar(
        textTitle = "Buscador de Maridos",
        onTextChanged = { textState = it },
        searchHint = "Ingrese el nombre del marido",
        textFieldValue = textState,
        onBackPressed = onBackPressed) {
        onSearch(textState.text)
    }
}