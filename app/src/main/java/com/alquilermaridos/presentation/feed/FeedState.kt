package com.alquilermaridos.presentation.feed

import com.alquilermaridos.domain.model.husband.HusbandData

data class FeedState(
    val husbandsData: List<HusbandData>? = null,
    val isLoading: Boolean = false,
    val error: String? = null
)