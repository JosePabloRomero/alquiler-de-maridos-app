package com.alquilermaridos.presentation.feed

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.alquilermaridos.R
import com.alquilermaridos.di.ResourcesProvider
import com.alquilermaridos.domain.model.husband.HusbandData
import com.alquilermaridos.domain.usecases.husband.GetHusbands
import com.alquilermaridos.util.Resource
import com.alquilermaridos.util.SnackBarManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FeedViewModel @Inject constructor(
    private var getHusbands: GetHusbands,
    private val snackbarManager: SnackBarManager,
    private val resourcesProvider: ResourcesProvider
) : ViewModel() {

    var state by mutableStateOf(FeedState())
        private set

    private var husbands: List<HusbandData>? = null

    init {
        fetchHusbands()
    }

    fun filterHusbandsByName(name: String) {
        val filteredHusbands = husbands?.filter { husband ->
            name.lowercase().trim() in husband.name.lowercase().trim()
        }
        filteredHusbands?.size?.let {
            if (it > 0) {
                state = state.copy(husbandsData = filteredHusbands)
            } else {
                snackbarManager.showMessage(resourcesProvider.getString(R.string.husbands_unmatched))
            }
        }
    }

    fun fetchHusbands() {
        viewModelScope.launch {
            state = state.copy(
                isLoading = true
            )
            val getHusbandsResult = async { getHusbands() }
            when (val result = getHusbandsResult.await()) {
                is Resource.Success -> {
                    husbands = result.data
                    state = state.copy(
                        husbandsData = husbands,
                        isLoading = false,
                        error = null
                    )
                }
                is Resource.Error -> {
                    state = state.copy(
                        husbandsData = null,
                        isLoading = false,
                        error = result.message
                    )
                    if (result.message != null) {
                        snackbarManager.showMessage(result.message)
                    } else {
                        snackbarManager.showMessage(resourcesProvider.getString(R.string.feed_error))
                    }
                }
            }
        }
    }

}